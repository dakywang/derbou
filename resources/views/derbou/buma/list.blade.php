@extends('layouts.app')

@section('content_header')
    @include('title')
    @include('breadcrumb')
@endsection

@section('content')
<style>
.fstElement{
    display:none
}
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <form id="form">
                    落布日期:
                    <input type="date" name="date" id="date" value="{{ $data['date'] }}">

                    <span class="btn btn-success" id="search">查詢</span>
                    <span class="btn btn-primary" id="reload">刷新頁面</span>
                    <span style="float:right" class="btn btn-danger" id="edit">修改紀錄</span>
                </form>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <tr class="head-color">
                        <th></th>
                        <th colspan="15">{{ $pageTitle }}</th>
                        <th>{{ $data['date'] }}</th>
                    </tr>
                    <tr class="head-color">
                        <th>序號</th>
                        <th>布號</th>
                        <th>機台號</th>
                        <th>落布月日</th>
                        <th>落布碼數</th>
                        <th>落布人</th>
                        @for ($i = 1; $i <= 8; $i++)
                        <th>{{ $i }}</th>
                        @endfor
                        <th>碼布員</th>
                        <th>擋台</th>
                        <th>師傅</th>
                    </tr>
                    @php
                    $index = 1;
                    @endphp
                    @foreach($data['data'] as $k => $v)
                    <tr>
                        <td>{{ $index }}</td>
                        <td>
                            <span>{{ $v['main']->bu_no }}</span>
                        </td>
                        <td>
                            <span>{{ $v['main']->machine_no }}</span>
                        </td>
                        <td>
                            <span>{{ $v['main']->data_date }}</span>
                        </td>
                        <td>
                            <span>{{ $v['main']->yard }}</span>
                        </td>
                        <td>
                            <span>{{ $data['user'][$v['main']->user_id]['name'] }}</span>
                        </td>
                        @for($i = 1; $i <= 8; $i++)
                            @if(isset($v['detail'][$i]))
                            <td>
                                <span class="detail-show">{{ $v['detail'][$i]['yard'] }}</span>
                                <input style="display: none" size="6" class="detail-show detail-edit" data-column="yard" data-bu_down_id="{{ $v['main']->id }}" data-serial_no="{{ $i }}"  value="{{ $v['detail'][$i]['yard'] }}">
                            </td>
                            @else
                            <td>
                                <input style="display: none" size="6" class="detail-show detail-edit" data-column="yard" data-bu_down_id="{{ $v['main']->id }}" data-serial_no="{{ $i }}"  value="">
                            </td>
                            @endif
                        @endfor
                        <td>
                            <span class="detail-show">
                            @if(!empty($v['main']->user_ma))
                                @foreach($v['main']->user_ma as $kk=>$vv)
                                    {{ $data['user'][$vv]['name'] }}
                                @endforeach
                            @endif
                            </span>
                            <select data-bu_down_id="{{ $v['main']->id }}" data-column="user_ma" style="display: none" class="multipleSelect ma-edit" multiple name="user_ma"  data-column="user_ma">
                                @foreach($data['user'] as $kk=>$vv)
                                    @if(!empty($v['main']->user_ma) && in_array($kk,$v['main']->user_ma) )
                                        <option selected value="{{ $kk }}">{{ $vv['name'] }}</option>
                                    @else
                                        <option value="{{ $kk }}">{{ $vv['name'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <span class="detail-show">
                            @if(!empty($v['main']->user_downtai))
                                @foreach($v['main']->user_downtai as $kk=>$vv)
                                    {{ $data['user'][$vv]['name'] }}
                                @endforeach
                            @endif
                            </span>
                            <select data-bu_down_id="{{ $v['main']->id }}" data-column="user_downtai" style="display: none" class="multipleSelect ma-edit" multiple name="user_downtai" data-column="user_downtai">
                                @foreach($data['user'] as $kk=>$vv)
                                    @if(!empty($v['main']->user_downtai) && in_array($kk,$v['main']->user_downtai) )
                                        <option selected value="{{ $kk }}">{{ $vv['name'] }}</option>
                                    @else
                                        <option value="{{ $kk }}">{{ $vv['name'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <span class="detail-show">
                            @if(!empty($v['main']->user_seafu))
                                @foreach($v['main']->user_seafu as $kk=>$vv)
                                    {{ $data['user'][$vv]['name'] }}
                                @endforeach
                            @endif
                            </span>
                            <select data-bu_down_id="{{ $v['main']->id }}" data-column="user_seafu" style="display: none" class="multipleSelect ma-edit" multiple name="user_seafu" data-column="user_seafu">
                                @foreach($data['user'] as $kk=>$vv)
                                    @if(!empty($v['main']->user_seafu) && in_array($kk,$v['main']->user_seafu) )
                                        <option selected value="{{ $kk }}">{{ $vv['name'] }}</option>
                                    @else
                                        <option value="{{ $kk }}">{{ $vv['name'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            <div class="box-footer">
                {{-- {{ $datas->links() }}
                <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">«</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">»</a></li>
                </ul> --}}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(function(){
        $('#search').on('click',function(){
            $('#form').submit();
        });
        $('#edit').on('click',function(){
            $(".detail-show").toggle();
            $(".fstElement").toggle();
            $('#machine_region').prop('disabled', function(i, v) { return !v; });
            $('#date').prop('disabled', function(i, v) { return !v; });
        });
        $('#reload').on('click',function(){
            location.reload();
        });
        $('.detail-edit').on('blur',function(){
            var obj = {};
            obj.bu_down_id = $(this).attr('data-bu_down_id');
            obj.serial_no = $(this).attr('data-serial_no');
            obj.value = $(this).val();;
            //console.log(obj);
            do_post('/derbou/buma/updateSerial',obj,function(data){
                console.log('ajax update detail success');
            });
        });
        $('.ma-edit').on('change',function(){
            var obj = {};
            obj.bu_down_id = $(this).attr('data-bu_down_id');
            obj.column = $(this).attr('data-column');
            obj.value = $(this).val();;
            console.log(obj);
            do_post('/derbou/buma/update',obj,function(data){
                console.log('ajax update detail success');
            });
        });
        $('.multipleSelect').fastselect();
    });
</script>
@endsection