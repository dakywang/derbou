@extends('layouts.app')

@section('content_header')
    @include('title')
    @include('breadcrumb')
@endsection

@section('content')
@php 
//var_dump($data['data']);
@endphp
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <form id="form">
                    日期:
                    <input type="date" name="date" id="date" value="{{ $data['date'] }}">
                    分區:
                    <select name="machine_region" id="machine_region">
                        <?php foreach (config('derbou.machine_region') as $key => $value) { ?>
                        <option {{ ($data['machine_region']==$key)?'selected':'' }} value="{{ $key }}">{{ $value }}</option>
                        <?php } ?>
                    </select>
                    <select name="time_region" id="time_region" style="display: none;">
                        <?php foreach (config('derbou.time_region') as $key => $value) { ?>
                        <option {{ ($data['time_region']==$key)?'selected':'' }} value="{{ $key }}">{{ $value }}</option>
                        <?php } ?>
                    </select>
                <span class="btn btn-success" id="search">查詢</span>
                <span class="btn btn-primary" id="reload">刷新頁面</span>
                <span style="float:right" class="btn btn-danger" id="edit">修改紀錄</span>
                </form>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <tr class="head-color">
                        <th></th>
                        <th colspan="4">
                            <span style="color:blue">{{ $data['machine_region'] }}區</span> 
                            {{-- <span style="color:blue" >{{ config('derbou.time_region')[$data['time_region']] }}</span>  --}}
                            {{ $pageTitle }}
                        </th>
                        <th class="detail-show" style="display: none"></th>
                        <th>{{ $data['date'] }}</th>
                    </tr>
                    <tr class="head-color">
                        <th>序號</th>
                        <th>布號</th>
                        <th>機台號</th>
                        <th>碼數</th>
                        <th>狀態</th>
                        <th>落布人</th>
                        <th class="detail-show" style="display: none">管理</th>
                    </tr>
                    @if(empty($data['data'])) 
                    <tr>
                        <td colspan="7" align="center">無資料</td>
                    </tr>
                    @else
                        @php 
                        $index = 1;
                        @endphp
                        @foreach($data['data'] as $k => $v) 
                        <tr>
                            <td>{{ $index }}</td>
                            <td>
                                <span class="detail-show">{{ $v->bu_no }}</span>
                                <input style="display: none" size="12" class="detail-show detail-edit" data-column="bu_no" data-id="{{ $v->id }}"  value="{{ $v->bu_no }}">
                            </td>
                            <td>
                                <span class="detail-show">{{ $v->machine_no }}</span>
                                <select style="display: none" class="detail-show detail-edit" data-id="{{ $v->id }}" data-column="machine_no" >
                                    <option value="">-</option>
                                    @foreach(config('derbou.machine_no')[$data['machine_region']] as $kk => $vv) 
                                        <option {{ ($kk==$v->machine_no)? 'selected': '' }} value="{{ $kk }}">{{ $kk }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <span class="detail-show">{{ $v->yard }}</span>
                                <input type="number" style="display: none" size="12" class="detail-show detail-edit" data-column="yard" data-id="{{ $v->id }}"  value="{{ $v->yard }}">
                            </td>
                            <td>
                                <span class="detail-show">{{ $v->kind }} {{ config('derbou.human_efficiency_kind')[$v->kind] }}</span>
                                <select style="display: none" class="detail-show detail-edit" data-column="kind" data-id="{{ $v->id }}">
                                    <option value="">-</option>
                                    @foreach(config('derbou.human_efficiency_kind') as $kk => $vv) 
                                        <option {{ ($kk==$v->kind)? 'selected': '' }} value="{{ $kk }}">{{ $kk }} {{ config('derbou.human_efficiency_kind')[$kk] }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <span class="detail-show">{{ $data['user'][$v->user_id]['name'] }}</span>
                                <select style="display: none" class="detail-show detail-edit" data-column="user_id" data-id="{{ $v->id }}">
                                    <option value="">-</option>
                                    @foreach($data['user'] as $kk => $vv)
                                        <option {{ ($kk==$v->user_id)? 'selected': '' }} value="{{ $kk }}">{{ $vv['name'] }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td class="detail-show" style="display: none">
                                <span data-id="{{ $v->id }}" class="btn btn-danger delete">刪除</span>
                            </td>
                        </tr>
                        @php 
                        $index++;
                        @endphp
                        @endforeach
                    @endif
                    <tr class="success">
                        <td>
                            <span class="btn btn-success" id="save">新增</span>
                        </td>
                        <td><input size="12" id="bu_no" /></td>
                        <td>
                            <select data-column="machine_no" id="machine_no">
                                <option value="">-</option>
                            @foreach(config('derbou.machine_no')[$data['machine_region']] as $k => $v) 
                                <option value="{{ $k }}">{{ $k }}</option>
                            @endforeach
                            </select>
                        </td>
                        <td><input size="10" type="number" id="yard"/></td>
                        <td>
                            <select id="kind">
                                <option value="">-</option>
                            @foreach(config('derbou.human_efficiency_kind') as $k => $v) 
                                <option value="{{ $k }}">{{ $k }} {{ $v }}</option>
                            @endforeach
                            </select>
                        </td>
                        <td>
                            <select data-column="user_id" id="user_id">
                                <option value="">-</option>
                            @foreach($data['user'] as $k => $v) 
                                <option value="{{ $v['id'] }}">{{ $v['name'] }}</option>
                            @endforeach
                            </select>
                        </td>
                        <td class="detail-show" style="display: none"></td>
                    </tr>
                </table>
            </div>
            <div class="box-footer">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(function(){
        $('#search').on('click',function(){
            $('#form').submit();
        });
        $('#edit').on('click',function(){
            $(".detail-show").toggle();
            $('#machine_region').prop('disabled', function(i, v) { return !v; });
            $('#date').prop('disabled', function(i, v) { return !v; });
        });
        $('#reload').on('click',function(){
            location.reload();
        });
        $('#save').on('click',function(){
            var error = "";
            var obj = {};
            obj.date = $('#date').val();
            obj.machine_region = $('#machine_region').val(); 
            obj.time_region = $('#time_region').val(); 
            obj.machine_no = $('#machine_no').val();
            obj.bu_no = $('#bu_no').val();
            obj.yard = $('#yard').val();
            obj.kind = $('#kind').val();
            obj.user_id = $('#user_id').val();

            if(obj.bu_no == "" || obj.machine_no == "" || obj.kind == "" || obj.user_id == "") error+="有欄位未填寫或選取";

            if(error !=""){
                alert(error)
            }else{
                do_post('/derbou/budown/save',obj,function(data){
                    console.log('ajax save detail success');
                    location.reload();
                });
                
            }
        });

        $('.delete').on('click',function(){
            var obj = {};
            obj.id = $(this).attr('data-id');

            if(confirm('是否要刪除??')){
                do_post('/derbou/budown/delete',obj,function(data){
                    console.log('ajax delete detail success');
                });
                location.reload();
            }
        });
        $('.detail-edit').on('blur',function(){
            var id = $(this).attr('data-id');
            var obj = {};
            obj.id = id;
            obj.column = $(this).attr('data-column');
            obj.value = $(this).val(); 
            console.log(obj);
            do_post('/derbou/budown/update',obj,function(data){
                console.log('ajax update detail success');
            });
        });
    });
</script>
@endsection