@extends('layouts.app')

@section('content_header')
    @include('title')
    @include('breadcrumb')
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <form id="form">
                    日期:
                    <input type="date" name="date" id="date" value="{{ $data['date'] }}">
                <span class="btn btn-success" id="search">查詢</span>
                <span class="btn btn-primary" id="reload">刷新頁面</span>
                <span style="float:right" class="btn btn-danger" id="edit">修改紀錄</span>
                </form>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <tr class="head-color">
                        <th></th>
                        <th colspan="7">
                            {{ $pageTitle }}
                        </th>
                        <th class="detail-show" style="display: none"></th>
                        <th>{{ $data['date'] }}</th>
                    </tr>
                    <tr class="head-color">
                        <th>序號</th>
                        <th>布號</th>
                        <th>機台號</th>
                        <th>落布時間</th>
                        <th class="warning">已烘碼數</th>
                        <th class="danger">未烘碼數</th>
                        <th>縮率(%)</th>
                        <th>狀態</th>
                        <th>落布人</th>
                        <th class="detail-show" style="display: none">管理</th>
                    </tr>
                    @if(empty($data['data']))
                    <tr>
                        <td colspan="7" align="center">無資料</td>
                    </tr>
                    @else
                        @php
                        $index = 1;
                        @endphp
                        @foreach($data['data'] as $k => $v)
                        <tr>
                            <td>{{ $index }}</td>
                            <td>
                                <span class="detail-show">{{ $v->bu_no }}</span>
                                <input style="display: none" size="12" class="detail-show detail-edit" data-column="bu_no" data-id="{{ $v->id }}"  value="{{ $v->bu_no }}">
                            </td>
                            <td>
                                <span class="detail-show">{{ $v->machine_no }}</span>
                                <select style="display: none" class="detail-show detail-edit" data-id="{{ $v->id }}" data-column="machine_no" >
                                    <option value="">-</option>
                                    @foreach(config('derbou.machine_no') as $kk => $vv)
                                    <optgroup label="區域{{ $kk }}">
                                        @foreach($vv as $kkk => $vvv)
                                        <option {{ ($kkk==$v->machine_no)? 'selected': '' }} value="{{ $kkk }}">{{ $kkk }}</option>
                                        @endforeach
                                    </optgroup>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <span class="detail-show">{{ $v->budown_date }}</span>
                                <input style="display: none" class="detail-show detail-edit" data-column="budown_date" type="date" name="budown_date" id="budown_date" data-id="{{ $v->id }}" value="{{ $v->budown_date }}">
                            </td>
                            <td class="warning">
                                <span class="detail-show">{{ $v->dry_yes }}</span>
                                <input type="number" style="display: none" size="12" class="detail-show detail-edit dry_count" data-column="dry_yes" data-id="{{ $v->id }}"  value="{{ $v->dry_yes }}">
                            </td>
                            <td class="danger">
                                <span class="detail-show">{{ $v->dry_no }}</span>
                                <input type="number" style="display: none" size="12" class="detail-show detail-edit dry_count" data-column="dry_no" data-id="{{ $v->id }}"  value="{{ $v->dry_no }}">
                            </td>
                            <td>
                                <span class="detail-show">{{ $v->dry_rate }}%</span>
                                <input type="number" style="display: none" size="12" class="detail-show detail-edit" data-column="dry_rate" data-id="{{ $v->id }}"  value="{{ $v->dry_rate }}">
                            </td>
                            <td>
                                <span class="detail-show">{{ $v->kind }} {{ !empty($v->kind)?config('derbou.human_efficiency_kind')[$v->kind]:'' }}</span>
                                <select style="display: none" class="detail-show detail-edit" data-column="kind" data-id="{{ $v->id }}">
                                    <option value="">-</option>
                                    @foreach(config('derbou.human_efficiency_kind') as $kk => $vv)
                                        <option {{ ($kk==$v->kind)? 'selected': '' }} value="{{ $kk }}">{{ $kk }} {{ config('derbou.human_efficiency_kind')[$kk] }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <span class="detail-show">{{ $data['user'][$v->user_id]['name'] }}</span>
                                <select style="display: none" class="detail-show detail-edit" data-column="user_id" data-id="{{ $v->id }}">
                                    <option value="">-</option>
                                    @foreach($data['user'] as $kk => $vv)
                                        <option {{ ($kk==$v->user_id)? 'selected': '' }} value="{{ $kk }}">{{ $vv['name'] }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td class="detail-show" style="display: none">
                                <span data-id="{{ $v->id }}" class="btn btn-danger delete">刪除</span>
                            </td>
                        </tr>
                        @php
                        $index++;
                        @endphp
                        @endforeach
                    @endif
                    <tr class="success">
                        <td>
                            <span class="btn btn-success" id="save">新增</span>
                        </td>
                        <td><input size="12" id="bu_no" /></td>
                        <td>
                            <select data-column="machine_no" id="machine_no">
                                <option value="">-</option>
                                @foreach(config('derbou.machine_no') as $kk => $vv)
                                    <optgroup label="區域{{ $kk }}">
                                        @foreach($vv as $kkk => $vvv)
                                        <option value="{{ $kkk }}">{{ $kkk }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </td>
                        <td><input type="date" name="budown_date" id="budown_date" value="{{ $data['date'] }}"></td>
                        <td class="warning"><input size="10" class="dry_count" type="number" id="dry_yes" data-column="dry_yes"/></td>
                        <td class="danger"><input size="10" class="dry_count" type="number" id="dry_no" data-column="dry_no"/></td>
                        <td><input size="10" type="number" id="dry_rate" data-column="dry_rate"/>%</td>
                        <td>
                            <select id="kind">
                                <option value="">-</option>
                            @foreach(config('derbou.human_efficiency_kind') as $k => $v)
                                <option value="{{ $k }}">{{ $k }} {{ $v }}</option>
                            @endforeach
                            </select>
                        </td>
                        <td>
                            <select data-column="user_id" id="user_id">
                                <option value="">-</option>
                            @foreach($data['user'] as $k => $v) 
                                <option value="{{ $v['id'] }}">{{ $v['name'] }}</option>
                            @endforeach
                            </select>
                        </td>
                        <td class="detail-show" style="display: none"></td>
                    </tr>
                </table>
            </div>
            <div class="box-footer">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(function(){
        $('.dry_count').on('change',function(){
            if($(this).attr('data-column')=='dry_yes'){
                var dry_yes = $(this).val()
                var dry_no = $(this).parent('td').next().children('input').val()

                if(dry_yes==undefined || dry_yes=='' || dry_no==undefined || dry_no==''){
                    $(this).parent('td').next().next().children('input').val(0)
                }else{
                    var rate = parseInt(dry_yes)/parseInt(dry_no)
                    rate = (1-rate)*100
                    rate = rate.toFixed(2)
                    $(this).parent('td').next().next().children('input').val(rate).trigger('blur')
                }
            }else{
                console.log($(this).closest('[data-column="dry_yes"]'))
                var dry_yes = $(this).parent('td').prev().children('input').val()
                var dry_no = $(this).val()

                if(dry_yes==undefined || dry_yes=='' || dry_no==undefined || dry_no==''){
                    $(this).parent('td').next().children('input').val(0)
                }else{
                    var rate = parseInt(dry_yes)/parseInt(dry_no)
                    rate = (1-rate)*100
                    rate = rate.toFixed(2)
                    $(this).parent('td').next().children('input').val(rate).trigger('blur')
                }
            }
        });
        $('#search').on('click',function(){
            $('#form').submit();
        });
        $('#edit').on('click',function(){
            $(".detail-show").toggle();
            $('#machine_region').prop('disabled', function(i, v) { return !v; });
            $('#date').prop('disabled', function(i, v) { return !v; });
        });
        $('#reload').on('click',function(){
            location.reload();
        });
        $('#save').on('click',function(){
            var error = "";
            var obj = {};
            obj.date = $('#date').val();
            obj.machine_no = $('#machine_no').val();
            obj.bu_no = $('#bu_no').val();
            obj.budown_date = $('#budown_date').val();
            obj.dry_yes = $('#dry_yes').val();
            obj.dry_no = $('#dry_no').val();
            obj.dry_rate = $('#dry_rate').val();
            obj.kind = $('#kind').val();
            obj.user_id = $('#user_id').val();

            if(obj.bu_no == "" || obj.machine_no == "" || obj.budown_date == ""|| obj.dry_yes == "" || obj.dry_no == "" || obj.dry_rate == "" || obj.user_id == "") error+="有欄位未填寫或選取";

            if(error !=""){
                alert(error)
            }else{
                do_post('/derbou/budry/save',obj,function(data){
                    console.log('ajax save detail success');
                    location.reload();
                });
            }
        });

        $('.delete').on('click',function(){
            var obj = {};
            obj.id = $(this).attr('data-id');

            if(confirm('是否要刪除??')){
                do_post('/derbou/budry/delete',obj,function(data){
                    console.log('ajax delete detail success');
                });
                location.reload();
            }
        });
        $('.detail-edit').on('blur',function(){
            var id = $(this).attr('data-id');
            var obj = {};
            obj.id = id;
            obj.column = $(this).attr('data-column');
            obj.value = $(this).val(); 
            console.log(obj);
            do_post('/derbou/budry/update',obj,function(data){
                console.log('ajax update detail success');
            });
        });
    });
</script>
@endsection