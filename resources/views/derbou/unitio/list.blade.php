@extends('layouts.app')

@section('content_header')
    @include('title')
    @include('breadcrumb')
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <form id="form">
                    起始日期:
                    <input type="date" name="bg_date" id="bg_date" value="{{ $data['bg_date'] }}">~
                    結束日期:
                    <input type="date" name="end_date" id="end_date" value="{{ $data['end_date'] }}">
                <span class="btn btn-success" id="search">查詢</span>
                <span class="btn btn-primary" id="reload">刷新頁面</span>
                </form>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <tr class="head-color">
                        <th></th>
                        <th colspan="11">
                            {{ $pageTitle }}
                        </th>
                        <th class="detail-show" style="display: none"></th>
                        <th colspan="1">{{ $data['bg_date'] }} ~ {{ $data['end_date'] }}</th>
                    </tr>
                    <tr class="head-color">
                        <th>進出日期</th>
                        <th>狀態</th>
                        <th>單號</th>
                        <th>廠商編號</th>
                        <th>貨品號</th>
                        <th>貨品名稱</th>
                        <th>數量</th>
                        <th>單位</th>
                        <th>單價</th>
                        <th>小計</th>
                        <th>操作人</th>
                        <th>備註</th>
                        <th>操作</th>
                    </tr>
                    @if(empty($data['data']))
                    <tr>
                        <td colspan="{{ count(config('derbou.bub_kind'))+2 }}" align="center">無資料</td>
                    </tr>
                    @else
                        @foreach($data['data'] as $k => $v)
                        <tr>
                            <td>{{ $v->data_date }}</td>
                            <td>{{ config('derbou.io_kind')[$v->io_kind] }}</td>
                            <td>{{ $v->unit_no }}</td>
                            <td>{{ $v->store_no }}</td>
                            <td>{{ $v->item_no }}</td>
                            <td>{{ $v->product_name }}</td>
                            <td>{{ $v->io_num }}</td>
                            <td>{{ $v->io_unit }}</td>
                            <td>{{ $v->item_price }}</td>
                            <td>{{ $v->io_num * $v->item_price }}</td>
                            <td>{{ $v->user_id }}</td>
                            <td>{{ $v->memo }}</td>
                            <td>
                                <span class="btn btn-warning">修改</span>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                </table>
                <br>
                <p>＊ 該報表會將相同批號的放在同一列</p>
                <br>
                <form id="saveForm">
                <table class="table table-bordered table-striped">
                    <tr class="success">
                        <th colspan="8">新增記錄</th>
                    </tr>
                    <tr class="primary">
                        <th>日期</th>
                        <th>布號</th>
                        <th>機台</th>
                        <th>批號</th>
                        <th>經緯紗</th>
                        <th>碼數</th>
                        <th>問題種類</th>
                        <th>功能</th>
                    </tr>
                    <tr>
                        <td><input type="date" size="12" name="data_date" id="data_date" /></td>
                        <td><input size="12" name="bu_no" id="bu_no" /></td>
                        <td>
                            <select name="machine_no" id="machine_no"  >
                                <option value="">-</option>
                                @foreach(config('derbou.machine_no') as $kk => $vv)
                                <optgroup label="區域{{ $kk }}">
                                    @foreach($vv as $kkk => $vvv)
                                    <option value="{{ $kkk }}">{{ $kkk }}</option>
                                    @endforeach
                                </optgroup>
                                @endforeach
                            </select>
                        </td>
                        <td><input size="12" name="pi_no" id="pi_no" /></td>
                        <td><input size="12" name="sa" id="sa" /></td>
                        <td><input type="number" size="12" name="yard" id="yard" /></td>
                        <td>
                            <select name="kind">
                            @foreach(config('derbou.bub_kind') as $k => $v)
                                <option value="{{ $k }}">{{ $k }} {{ $v }}</option>
                            @endforeach
                            <select>
                        </td>
                        <td><span class="btn btn-success" id="save">新增</span></td>
                    </tr>
                </table>
                </form>
            </div>
            <div class="box-footer">
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">修改/刪除資料</h5>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button style="float:left" type="button" class="btn btn-danger" id="delete">刪除</button>
        <button type="button" class="btn btn-primary" id="update">送出修改</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal end -->
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(function(){
        $('#search').on('click',function(){
            $('#form').submit();
        });
        $('.edit').on('click',function(){
            var obj = JSON.parse($(this).attr('data-json'));
            for(var item in obj){
                if(item=='machine_no' || item=='kind'){
                    $('#editForm select[name='+item+'] option[value='+obj[item]+']').attr('selected','selected');
                }else{
                    $('#editForm input[name='+item+']').val(obj[item])
                }
            }
            $('#modal').modal('show')
        });
        $('#reload').on('click',function(){
            location.reload();
        });
        $('#save').on('click',function(){
            var error = "";
            var obj = $('#saveForm').serializeObject();

            if(obj.data_date == "" || obj.bu_no == "" || obj.machine_no == ""|| obj.pi_no == "" || obj.sa == "" || obj.yard == "" || obj.kind == "") error+="有欄位未填寫或選取";
            if(error !=""){
                alert(error)
            }else{
                do_post('/derbou/bub/save',obj,function(data){
                    console.log('ajax save detail success');
                    location.reload();
                });
            }
        });

        $('#delete').on('click',function(){
            var obj = $('#editForm').serializeObject();

            if(confirm('是否要刪除??')){
                do_post('/derbou/bub/delete',obj,function(data){
                    console.log('ajax delete detail success');
                    location.reload();
                });
            }
        });
        $('#update').on('click',function(){
            var error = ""
            var obj = $('#editForm').serializeObject();
            if(obj.data_date == "" || obj.bu_no == "" || obj.machine_no == ""|| obj.pi_no == "" || obj.sa == "" || obj.yard == "" || obj.kind == "") error+="有欄位未填寫或選取";
            if(error !=""){
                alert(error)
            }else{
                do_post('/derbou/bub/update',obj,function(data){
                    console.log('ajax save detail success');
                    location.reload();
                });
            }
        });
    });
</script>
@endsection