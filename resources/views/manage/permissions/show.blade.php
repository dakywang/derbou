@extends('layouts.app')

@section('content_header')
    @include('title')
    @include('breadcrumb')
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="btn-group pull-right">
                    <a href="{{ url('manage/permissions/edit/' . $permissions->id) }}" class="btn btn-default">編輯</a>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('manage/permissions/destroy/' . $permissions->id) }}" class="destroy-permission">刪除權限</a></li>
                    </ul>
                </div>
                @if ($permissions->display_name)
                <h4>{{ $permissions->display_name }} <small>{{ $permissions->name }}</small></h4>
                @else
                <h4>{{ $permissions->name }}</h4>
                @endif
            </div>
            <div class="box-body">
                @if ($permissions->description)
                    <p>{{ $permissions->description }}</p>
                @endif
                @if ($permissions->route_name)
                    <p>{{ $permissions->route_name }}</p>
                @endif
            </div>
            {{-- <div class="box-footer">
            </div> --}}
        </div>
        <div class="box">
            <div class="box-header"><h4>相關職務列表</h4></div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>名稱</th>
                                <th>顯示名稱</th>
                                <th>敘述</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($roles as $role)
                            <tr>
                                <td>{{ $role->name }}</td>
                                <td><a href="{{ URL::to('manage/roles/show/' . $role->id) }}">{{ $role->display_name }}</a></td>
                                <td>{{ $role->description }}</td>
                            </tr>
                            @endforeach
                            @if (count($roles) == 0)
                            <tr>
                                <td colspan="4" class="text-center text-muted">尚無職務</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $('.destroy-permission').click(function(event) {
        @if (count($roles) == 0)
        if (!confirm('確認要刪除此權限？')) {
            return false;
        }
        @else
        alert('此權限還有相關帳號，不可刪除！');
        return false;
        @endif
    });
</script>
@endsection