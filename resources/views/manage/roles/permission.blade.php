@extends('layouts.app')

@section('content_header')
    @include('title')
    @include('breadcrumb')
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                @if ($role->display_name)
                <h4>{{ $role->display_name }} <small>{{ $role->name }}</small></h4>
                @else
                <h4>{{ $role->name }}</h4>
                @endif
            </div>
            <div class="box-body">
                @if ($role->description)
                    <p>{{ $role->description }}</p>
                @else
                    <p class="text-muted">尚無描述</p>
                @endif
            </div>
            {{-- <div class="box-footer">
            </div> --}}
        </div>
        <div class="box">
            <div class="box-header"><h4>相關權限列表</h4></div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>管理</th>
                                <th>權限名稱</th>
                                <th>權限描述</th>
                                <th>權限路徑</th>
                                <th>權限ID(程式用)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <form id="form" role="form" method="POST" action="{{ url('/manage/roles/permission/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="role_id" value="{{ $role->id }}">
                            @foreach ($permissions as $permission)
                            <tr>
                                <td>
                                    @php
                                    $have_right = false;
                                    foreach($permission_role as $k => $v){
                                        if($v->permission_id == $permission->id) $have_right = true;
                                    }
                                    @endphp
                                    <input type="checkbox" name="permission[]" value="{{ $permission->id }}" {{ ($have_right)? 'checked':'' }} />
                                </td>
                                <td>{{ $permission->name }}</td>
                                <td>{{ $permission->description }}</td>
                                <td>{{ $permission->route_name }}</td>
                                <td>{{ $permission->id }}</td>
                            </tr>
                            @endforeach
                            @if (count($permissions) == 0)
                            <tr>
                                <td colspan="5" class="text-center text-muted">尚無建立任何權限</td>
                            </tr>
                            @endif
                            <tr>
                                <td colspan="5"><span class="btn btn-success center" id="submit">儲存 & 送出</span></td>
                            </tr>
                            </form>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $('#submit').click(function(event) {
        if(confirm('確定要修改權限??')){
            $('#form').submit()
        }
    });
</script>
@endsection