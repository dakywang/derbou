<h1>
    {{ $pageTitle or '首頁' }}
    @if(isset($subTitle)) <small>{{ $subTitle }}</small> @endif
</h1>