<?php

namespace ERP\Model\Derbou;

use Illuminate\Database\Eloquent\Model;
use DB;

class BuDown extends Model
{
	protected $table = 'bu_down';

    protected $fillable = [
        'bu_no','data_date','time_region','machine_no','yard', 'kind', 'user_id',
    ];

    public function getDataByDateAndTimeRegion($date , $machine_region = 'A', $time_region = 'morning'){
    	$res = array();
    	
    	//init
    	$res['date'] = $date;
    	$res['machine_region'] = $machine_region;
        $res['time_region'] = $time_region;
    	$res['data'] = array();

    	$result = DB::table($this->table.' AS a')
    	->select(DB::raw('a.* , b.name'))
    	->where('a.data_date',$date)
        ->where('a.time_region',$time_region)
        ->where('a.machine_region',$machine_region)
    	->leftJoin('users AS b','a.user_id','b.id')
    	->get();

    	foreach ($result as $k => $v) {
            $res['data'][$v->id] = $v;
    	}

    	return $res;
    }
}
