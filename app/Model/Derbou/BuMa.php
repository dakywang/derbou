<?php

namespace ERP\Model\Derbou;

use Illuminate\Database\Eloquent\Model;
use DB;

class BuMa extends Model
{
	protected $table = 'bu_ma';

    protected $fillable = [
        'bu_down_id','user_ma','user_downtai', 'user_seafu', 'user_id'
    ];
}
