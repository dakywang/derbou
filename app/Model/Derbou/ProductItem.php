<?php

namespace ERP\Model\Derbou;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductItem extends Model
{
	protected $table = 'product_item';
}
