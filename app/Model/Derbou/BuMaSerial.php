<?php

namespace ERP\Model\Derbou;

use Illuminate\Database\Eloquent\Model;
use DB;

class BuMaSerial extends Model
{
	protected $table = 'bu_ma_serial';

    protected $fillable = [
        'bu_down_id','serial_no', 'yard', 'user_id'
    ];
}
