<?php

namespace ERP\Model\Derbou;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductUnit extends Model
{
	protected $table = 'product_unit';
}
