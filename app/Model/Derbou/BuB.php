<?php

namespace ERP\Model\Derbou;

use Illuminate\Database\Eloquent\Model;
use DB;

class BuB extends Model
{
	protected $table = 'bu_b';

    protected $fillable = [
        'data_date','bu_no', 'machine_no','pi_no','sa','yard','kind', 'user_id'
    ];
}
