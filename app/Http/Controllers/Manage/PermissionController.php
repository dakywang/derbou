<?php

namespace ERP\Http\Controllers\Manage;

use ERP\Model\BreadCrumb;
use ERP\Model\Manage\Permission;
use ERP\Model\Manage\Role;
use ERP\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    protected function index()
    {
        // DB::enableQueryLog();
        $permissions = DB::table('permissions')
            ->select(DB::raw('permissions.*, count(permission_role.role_id) as roles'))
            ->leftJoin('permission_role', 'permissions.id', '=', 'permission_role.permission_id')
            ->groupBy('permissions.id')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);
        
        return View::make('manage.permissions.list', [
            'permissions' => $permissions,
            'pageTitle'   => '權限管理',
            'subTitle'    => '權限清單',
            'breadcrumbs' => $this->getBreadCrumb('index'),
        ]);
    }
    protected function create()
    {
        $permission_kinds = DB::table('permission_kinds')->get();
            //dd($permission_kinds);
        return View::make('manage.permissions.create', [
            'permission_kinds' => $permission_kinds,
            'pageTitle'   => '權限管理',
            'subTitle'    => '新增權限',
            'breadcrumbs' => $this->getBreadCrumb('create'),
        ]);
    }
    protected function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'role_name'    => 'required|max:255',
            'display_name' => 'max:255',
            'description'  => 'max:255',
            'route_name'  => 'max:255',
        ]);

        if ($validator->fails()) {
            return redirect('manage/permissions/create')
                ->withErrors($validator)
                ->withInput();
        }

        $insertId = Permission::create([
            'name'         => $data['role_name'],
            'display_name' => $data['display_name'] ?: null,
            'description'  => $data['description'] ?: null,
            'route_name'  => $data['route_name'] ?: null,
            'permission_kind_id'  => $data['permission_kind_id'] ?: null,
        ])->id;

        return redirect()->route('manage.permissions.show', ['id' => $insertId]);
    }
    protected function edit($id)
    {
        $permission = DB::table('permissions')
            ->find($id);
        $permission_kinds = DB::table('permission_kinds')->get();

        return View::make('manage.permissions.edit', [
            'permission'  => $permission,
            'permission_kinds'  => $permission_kinds,
            'pageTitle'   => '權限管理',
            'subTitle'    => '權限編輯',
            'breadcrumbs' => $this->getBreadCrumb('edit', $id),
        ]);
    }
    protected function update(Request $request)
    {
        $data       = $request->all();
        $mId        = $data['id'] or '';

        $validator = Validator::make($data, [
            'permission_name'    => 'required|max:255',
            'display_name' => 'max:255',
            'description'  => 'max:255',
            'route_name'  => 'max:255',
        ]);

        if ($validator->fails()) {
            return redirect('manage/permissions/edit/' . $mId)
                ->withErrors($validator)
                ->withInput();
        }

        $role = Permission::find($mId);
        $role->name = $data['permission_name'];
        $role->display_name = $data['display_name'] ?: null;
        $role->description = $data['description'] ?: null;
        $role->route_name = $data['route_name'] ?: null;
        $role->permission_kind_id = $data['permission_kind_id'] ?: null;
        $role->save();

        return redirect()->route('manage.permissions.show', ['id' => $mId]);
    }
    protected function show($id)
    {
        $permissions = DB::table('permissions')->find($id);
        $roles = DB::table('permission_role')
            ->select(DB::raw('roles.*'))
            ->leftJoin('roles', 'roles.id', '=', 'permission_role.role_id')
            ->where('permission_role.permission_id', '=', $id)
            ->get();
        // DB::enableQueryLog();
        // dd(DB::getQueryLog());
        // dd($users);
        return View::make('manage.permissions.show', [
            'permissions'   => $permissions,
            'roles'   => $roles,
            'pageTitle'   => '權限管理',
            'subTitle'    => '權限詳情',
            'breadcrumbs' => $this->getBreadCrumb('show'),
        ]);
    }
    protected function destroy($id)
    {
        $relatedRoles = DB::table('permission_role')
            ->where('permission_id', '=', $id)
            ->get();
        if (count($relatedRoles)) {
            return redirect('manage/permissions/show/' . $id);
        }
        $role = DB::table('permissions')
            ->where('id', '=', $id);
        $role->delete();
        return redirect('manage/permissions');
    }

    private function getBreadCrumb($page = '', $id = 0)
    {
        $homeBreadCrumb        = new BreadCrumb();
        $homeBreadCrumb->href  = url('/');
        $homeBreadCrumb->title = "首頁";

        $createBreadCrumb        = new BreadCrumb();
        $createBreadCrumb->title = "新增權限";

        $listBreadCrumb        = new BreadCrumb();
        $listBreadCrumb->title = "權限清單";

        $showBreadCrumb        = new BreadCrumb();
        $showBreadCrumb->title = "權限詳情";

        $editBreadCrumb        = new BreadCrumb();
        $editBreadCrumb->title = "權限編輯";

        switch ($page) {
            case 'index':
                $breadcrumbs = [$homeBreadCrumb, $listBreadCrumb];
                break;
            case 'create':
                $listBreadCrumb->href = url('/manage/permissions');
                $breadcrumbs              = [$homeBreadCrumb, $listBreadCrumb, $createBreadCrumb];
                break;
            case 'show':
                $listBreadCrumb->href = url('/manage/permissions');
                $breadcrumbs              = [$homeBreadCrumb, $listBreadCrumb, $showBreadCrumb];
                break;
            case 'edit':
                $listBreadCrumb->href = url('/manage/permissions');
                $showBreadCrumb->href = url('/manage/permissions/show/' . $id);
                $breadcrumbs              = [$homeBreadCrumb, $listBreadCrumb, $showBreadCrumb, $editBreadCrumb];
                break;
            default:
                $breadcrumbs = [];
                break;
        }

        return $breadcrumbs;
    }
}
