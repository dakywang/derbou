<?php

namespace ERP\Http\Controllers\Derbou;

use ERP\Model\BreadCrumb;
use ERP\Model\Manage\User;
use ERP\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use ERP\Model\Derbou\BuMa;
use ERP\Model\Derbou\BuMaSerial;
use ERP\Model\Derbou\BuDown;

class BuMaController extends Controller
{
	protected $date = '';

	public function __construct()
    {
        $this->date = dateadd(config('const.today'),-1);
    }
   	protected function index(Request $request){
        $date = ($request->input('date')==null) ? $this->date : $request->input('date');

        $data = array();
        $data['date'] = $date;

        //$budown = BuDown::where('data_date',$date)->get();
        $budown = DB::table('bu_down')
            ->select(DB::raw('bu_down.*, bu_ma.user_ma,bu_ma.user_downtai, bu_ma.user_seafu'))
            ->leftJoin('bu_ma', 'bu_down.id', '=', 'bu_ma.bu_down_id')
            ->where('bu_down.data_date','=',$date)
            ->get();

        $data['data'] = array();
        foreach($budown as $key => $obj){
            $obj->user_ma = json_decode($obj->user_ma,true);
            $obj->user_downtai = json_decode($obj->user_downtai,true);
            $obj->user_seafu = json_decode($obj->user_seafu,true);
            $data['data'][$obj->id]['main'] = $obj;
            //
            $buma = BuMaSerial::where('bu_down_id',$obj->id)->get();
            foreach($buma as $k => $v){
                $data['data'][$obj->id]['detail'][$v['serial_no']] = $v;
            }
        }

        //build users
        $data['user'] = array();
	    $users = User::all();
        $users = $users->toArray();
        foreach ($users as $k => $v) {
            $data['user'][$v['id']] = $v;
        }
        //dd($data['user']);

		return view('derbou.buma.list', [
            'data'       => $data,
            'pageTitle'   => '碼布日報表',
            'subTitle'    => '清單',
            'breadcrumbs' => $this->getBreadCrumb('index'),
        ]);
   	}
    protected function update(Request $request){
        if(!$request->value || empty($request->value)){
            $request->value = '';
        }else{
            $request->value = json_encode($request->value);
        }
        $result = BuMa::updateOrCreate(
                ['bu_down_id'=>$request->bu_down_id],
                [$request->column=>$request->value
                , 'user_id'=>session('user.id')]);

        return response()->json([ 'ok' => $request ]);
    }
    protected function updateSerial(Request $request){
        if($request->value === ''){
            BuMaSerial::where('bu_down_id', '=', $request->bu_down_id)->where('serial_no', '=', $request->serial_no)->delete();
        }else{
            $result = BuMaSerial::updateOrCreate(
                ['bu_down_id'=>$request->bu_down_id
                ,'serial_no'=>$request->serial_no],
                ['yard'=>$request->value
                , 'user_id'=>session('user.id')]);
        }
        return response()->json([ 'ok' => true ]);
    }
   	private function getBreadCrumb($page = '', $id = 0)
    {
        $homeBreadCrumb        = new BreadCrumb();
        $homeBreadCrumb->href  = url('/');
        $homeBreadCrumb->title = "首頁";

        $listBreadCrumb        = new BreadCrumb();
        $listBreadCrumb->title = "資料清單";

        switch ($page) {
            case 'index':
                $breadcrumbs = [$homeBreadCrumb, $listBreadCrumb];
                break;
            default:
                $breadcrumbs = [];
                break;
        }

        return $breadcrumbs;
    }
}
