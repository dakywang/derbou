<?php

namespace ERP\Http\Controllers\Derbou;

use ERP\Model\BreadCrumb;
use ERP\Model\Manage\User;
use ERP\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use ERP\Model\Derbou\BuB;

class BuBController extends Controller
{
	protected $date = '';

	public function __construct()
    {
        $this->date = dateadd(config('const.today'),-1);
    }
   	protected function index(Request $request){
        $bg_date = ($request->input('bg_date')==null) ? dateadd($this->date,-30) : $request->input('bg_date');
        $end_date = ($request->input('end_date')==null) ? $this->date : $request->input('end_date');

	    $data = array();
        $data['bg_date'] = $bg_date;
        $data['end_date'] = $end_date;

        $data['data'] = array();
        $bub = BuB::where('data_date','>=',$bg_date)->where('data_date','<=',$end_date)->get();
        foreach($bub as $key => $obj){
            if(!isset($data['data'][$obj['pi_no']])){ //批號不存在
                $data['data'][$obj['pi_no']]['main'] = $obj;
                foreach(config('derbou.bub_kind') as $k => $v){
                    $data['data'][$obj['pi_no']][$k] = array();
                }
            }
            $data['data'][$obj['pi_no']][$obj['kind']][] = $obj;
        }
        //dd($data);

        //build users
        $data['user'] = array();
	    $users = User::all();
        $users = $users->toArray();
        foreach ($users as $k => $v) {
            $data['user'][$v['id']] = $v;
        }
        //dd($data['user']);

		return view('derbou.bub.list', [
            'data'       => $data,
            'pageTitle'   => 'B級布管制日報表',
            'subTitle'    => '清單',
            'breadcrumbs' => $this->getBreadCrumb('index'),
        ]);
   	}

    protected function save(Request $request){

        $result = new BuB;
        $result->data_date = $request->data_date;
        $result->bu_no = $request->bu_no;
        $result->machine_no = $request->machine_no;
        $result->pi_no = $request->pi_no;
        $result->sa = $request->sa;
        $result->yard = $request->yard;
        $result->kind = $request->kind;
        $result->save();

        return response()->json([ 'ok' => true ]);
    }
    
    protected function update(Request $request){
        $input = $request->all();
        $result = BuB::find($request->id);
        foreach($input as $k => $v){
            $result->$k = $v;
        }
        $result->save();
        return response()->json([ 'ok' => true ]);
    }

    protected function delete(Request $request){
        BuB::destroy($request->id);
        return response()->json([ 'ok' => true ]);
    }

   	private function getBreadCrumb($page = '', $id = 0)
    {
        $homeBreadCrumb        = new BreadCrumb();
        $homeBreadCrumb->href  = url('/');
        $homeBreadCrumb->title = "首頁";

        $listBreadCrumb        = new BreadCrumb();
        $listBreadCrumb->title = "資料清單";

        switch ($page) {
            case 'index':
                $breadcrumbs = [$homeBreadCrumb, $listBreadCrumb];
                break;
            default:
                $breadcrumbs = [];
                break;
        }

        return $breadcrumbs;
    }
}
