<?php

namespace ERP\Http\Controllers\Derbou;

use ERP\Model\BreadCrumb;
use ERP\Model\Manage\User;
use ERP\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use ERP\Model\Derbou\BuDry;

class BuDryController extends Controller
{
	protected $date = '';

	public function __construct()
    {
        $this->date = dateadd(config('const.today'),-1);
    }
   	protected function index(Request $request){
   		$date = ($request->input('date')==null) ? $this->date : $request->input('date');
   		$class = new BuDry();
	    $data = array();
        $data['date'] = $date;

        $data['data'] = BuDry::where('data_date',$date)->get();

        //build users
        $data['user'] = array();
	    $users = User::all();
        $users = $users->toArray();
        foreach ($users as $k => $v) {
            $data['user'][$v['id']] = $v;
        }
        //dd($data['user']);

		return view('derbou.budry.list', [
            'data'       => $data,
            'pageTitle'   => '烘布日報表',
            'subTitle'    => '清單',
            'breadcrumbs' => $this->getBreadCrumb('index'),
        ]);
   	}

    protected function save(Request $request){
        $result = new BuDry;
        $result->data_date = $request->date;
        $result->bu_no = $request->bu_no;
        $result->machine_no = $request->machine_no;
        $result->dry_yes = $request->dry_yes;
        $result->dry_no = $request->dry_no;
        $result->dry_rate = $request->dry_rate;
        $result->budown_date = $request->budown_date;
        $result->kind = $request->kind;
        $result->user_id = intval($request->user_id);
        $result->save();

        return response()->json([ 'ok' => true ]);
    }
    
    protected function update(Request $request){
        $result = BuDry::find($request->id);
        $c = $request->column;
        $result->$c = $request->value;
        $result->save();
        return response()->json([ 'ok' => true ]);
    }

    protected function delete(Request $request){
        BuDry::destroy($request->id);
        return response()->json([ 'ok' => true ]);
    }

   	private function getBreadCrumb($page = '', $id = 0)
    {
        $homeBreadCrumb        = new BreadCrumb();
        $homeBreadCrumb->href  = url('/');
        $homeBreadCrumb->title = "首頁";

        $listBreadCrumb        = new BreadCrumb();
        $listBreadCrumb->title = "資料清單";

        switch ($page) {
            case 'index':
                $breadcrumbs = [$homeBreadCrumb, $listBreadCrumb];
                break;
            default:
                $breadcrumbs = [];
                break;
        }

        return $breadcrumbs;
    }
}
