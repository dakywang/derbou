<?php

namespace ERP\Http\Controllers\Derbou;

use ERP\Model\BreadCrumb;
use ERP\Model\Manage\User;
use ERP\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use ERP\Model\Derbou\BuDown;

class BuDownController extends Controller
{
	protected $date = '';
	protected $machine_region = 'A';
    protected $time_region = 'morning';

	public function __construct()
    {
        $this->date = dateadd(config('const.today'),-1);
    }
   	protected function index(Request $request){
   		$date = ($request->input('date')==null) ? $this->date : $request->input('date');
   		$machine_region = ($request->input('machine_region')==null) ? $this->machine_region : $request->input('machine_region');
        $time_region = ($request->input('time_region')==null) ? $this->time_region : $request->input('time_region');
   		$class = new BuDown();
	    $data = $class->getDataByDateAndTimeRegion($date,$machine_region,$time_region);

        //build users
        $data['user'] = array();
	    $users = User::all();
        $users = $users->toArray();
        foreach ($users as $k => $v) {
            $data['user'][$v['id']] = $v;
        }
        //dd($data['user']);

		return view('derbou.budown.list', [
            'data'       => $data,
            'pageTitle'   => '落布日報表',
            'subTitle'    => '清單',
            'breadcrumbs' => $this->getBreadCrumb('index'),
        ]);
   	}

    protected function save(Request $request){
        $result = new BuDown;
        $result->data_date = $request->date;
        $result->bu_no = $request->bu_no;
        $result->time_region = $request->time_region;
        $result->machine_no = $request->machine_no;
        $result->machine_region = $request->machine_region;
        $result->yard = $request->yard;
        $result->kind = $request->kind;
        $result->user_id = intval($request->user_id);
        $result->save();

        return response()->json([ 'ok' => true ]);
    }
    
    protected function update(Request $request){
        $result = BuDown::find($request->id);
        $c = $request->column;
        $result->$c = $request->value;
        $result->save();
        /*if('detail'==$request->action){
            $result = BuDown::updateOrCreate(
                ['data_date'=>$request->date
                ,'time_region'=>$request->time_region
                ,'machine_no'=>$request->machine_no
                ,'machine_region'=>$request->machine_region
                , 'user_id'=>intval($request->user_id)],
                ['yard'=>$request->yard
                , 'kind'=>$request->kind
                , 'bu_no'=>$request->bu_no]);
            return response()->json([ 'ok' => true ]);
        }else{
            return response()->json(['ok' => false ]);
        }*/
        return response()->json([ 'ok' => true ]);
    }

    protected function delete(Request $request){
        BuDown::destroy($request->id);
        return response()->json([ 'ok' => true ]);
    }
    
   	private function getBreadCrumb($page = '', $id = 0)
    {
        $homeBreadCrumb        = new BreadCrumb();
        $homeBreadCrumb->href  = url('/');
        $homeBreadCrumb->title = "首頁";

        $listBreadCrumb        = new BreadCrumb();
        $listBreadCrumb->title = "資料清單";

        switch ($page) {
            case 'index':
                $breadcrumbs = [$homeBreadCrumb, $listBreadCrumb];
                break;
            default:
                $breadcrumbs = [];
                break;
        }

        return $breadcrumbs;
    }
}
