<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('store', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data_date'); //進出日期
            $table->string('name')->nullable(); //備註
            $table->string('memo')->nullable(); //備註
            $table->integer('user_id')->unsigned(); //建立使用者
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('store');
    }
}
