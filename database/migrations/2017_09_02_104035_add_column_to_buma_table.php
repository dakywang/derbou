<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToBumaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //Schema::rename('bu_ma', 'bu_ma');
        Schema::table('bu_ma', function($table) {
            $table->dropColumn('yard');
            $table->dropColumn('serial_no');
            $table->string('user_ma')->nullable();
            $table->string('user_downtai')->nullable();
            $table->string('user_seafu')->nullable();
        });
        Schema::create('bu_ma_serial', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bu_down_id')->unsigned();
            $table->integer('yard')->unsigned();
            $table->integer('serial_no')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('bu_ma', function($table) {
            $table->integer('serial_no')->unsigned();
            $table->integer('yard')->unsigned();
            $table->dropColumn('user_ma');
            $table->dropColumn('user_downtai');
            $table->dropColumn('user_seafu');
        });
        Schema::dropIfExists('bu_ma_serial');
    }
}
