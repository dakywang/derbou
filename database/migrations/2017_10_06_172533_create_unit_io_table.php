<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitIoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('unit_io', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data_date'); //進出日期
            $table->string('unit_no')->nullable(); //單號
            $table->string('store_no')->nullable(); //廠商編號
            $table->string('item_no')->nullable(); //貨品號
            $table->integer('item_price')->unsigned(); //貨品小計
            $table->integer('io_num')->unsigned(); //數量
            $table->string('io_unit')->nullable(); //單位
            $table->string('io_kind')->nullable(); //進or出貨
            $table->string('memo')->nullable(); //備註
            $table->integer('user_id')->unsigned(); //操作使用者
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('unit_io');
    }
}
