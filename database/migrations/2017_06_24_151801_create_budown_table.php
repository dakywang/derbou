<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudownTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bu_down', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data_date');
            $table->string('bu_no')->nullable();
            $table->string('time_region')->nullable();
            $table->string('machine_no')->nullable();
            $table->string('machine_region')->nullable();
            $table->string('yard')->nullable();
            $table->string('kind')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bu_down');
    }
}
