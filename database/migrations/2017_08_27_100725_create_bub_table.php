<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('bu_b', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data_date');
            $table->string('bu_no')->nullable();
            $table->string('machine_no')->nullable();
            $table->string('pi_no')->nullable();
            $table->string('sa')->nullable();
            $table->integer('yard')->unsigned();
            $table->string('kind')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('bu_b');
    }
}
