<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('bu_dry', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data_date');
            $table->string('bu_no')->nullable();
            $table->string('machine_no')->nullable();
            $table->date('budown_date');
            $table->string('dry_no')->nullable();
            $table->string('dry_yes')->nullable();
            $table->string('dry_rate')->nullable(); //縮率
            $table->string('budown_user')->nullable();
            $table->string('kind')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('bu_dry');
    }
}
