<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('product_item', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_no');
            $table->date('data_date'); //建立日期
            $table->string('name')->nullable(); //備註
            $table->string('memo')->nullable(); //備註
            $table->integer('user_id')->unsigned(); //建立使用者
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('product_item');
    }
}
